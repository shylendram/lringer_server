package com.geeklabs.lringer.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.lringer.dto.TrackDto;
import com.geeklabs.lringer.dto.UserDto;
import com.geeklabs.lringer.service.UserService;
import com.geeklabs.lringer.util.ResponseStatus;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	private UserService userService;
	
	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus regiser(@RequestBody UserDto userDto) throws IOException {
		return userService.registerUser(userDto);
	}

	@RequestMapping(value = "/updateLocation/{userId}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus updateLoc(@RequestBody TrackDto trackDto, @PathVariable long userId) throws IOException {
		return userService.updateLocation(trackDto, String.valueOf(userId));
	}
	
	@RequestMapping(value = "/signout/{userId}", method = RequestMethod.GET)
	public @ResponseBody ResponseStatus signOut(@PathVariable long userId) {
		return userService.updateSigninStatus(userId);
	}
	
	@RequestMapping(value = "/getLocation/{mblNum}", method = RequestMethod.GET)
	@ResponseBody
	public UserDto getLocationByMobileNumber(@PathVariable String mblNum) {
		return userService.getLocationByMobileNumber(mblNum);
	}
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
