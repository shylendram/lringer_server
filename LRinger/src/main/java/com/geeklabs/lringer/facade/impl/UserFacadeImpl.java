package com.geeklabs.lringer.facade.impl;

import java.util.Date;

import com.geeklabs.lringer.domain.User;
import com.geeklabs.lringer.dto.UserDto;
import com.geeklabs.lringer.facade.UserFacade;
import com.google.api.services.oauth2.model.Userinfo;

public class UserFacadeImpl implements UserFacade {

	@Override
	public User convertToUserDomain(Userinfo userInfo, UserDto userDto) {
		User user = new User();
		user.setEmail(userInfo.getEmail());
		user.setMobileNumber("+91" + userDto.getMobileNumber());
		user.setRegisteredDate(new Date());
		user.setSignIn(true);
		user.setName(userInfo.getName());
		user.setAccessToken(userDto.getAccessToken());
		return user;
	}

	@Override
	public UserDto convertToUserDto(User user) {
		UserDto userDto = new UserDto();
		userDto.setId(user.getId());
		userDto.setMobileNumber(user.getMobileNumber());
		userDto.setAddress(user.getAddress());
		userDto.setCity(user.getCity());
		userDto.setEmail(user.getEmail());
		return userDto;
	}

	
}
