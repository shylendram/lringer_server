package com.geeklabs.lringer.facade;

import com.geeklabs.lringer.domain.User;
import com.geeklabs.lringer.dto.UserDto;
import com.google.api.services.oauth2.model.Userinfo;

public interface UserFacade {

	User convertToUserDomain(Userinfo userInfo, UserDto userDto);
	UserDto convertToUserDto(User user);
}
