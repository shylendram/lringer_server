package com.geeklabs.lringer.domain;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Index;

@Entity
public class UserPreferences {

	@com.googlecode.objectify.annotation.Id
	private Long Id;

	private Float distance;

	private int time;
	
	@Index
	private Key<User> user;

	public Long getId() {
		return Id;
	}

	public Float getDistance() {
		return distance;
	}

	public void setDistance(Float distance) {
		this.distance = distance;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public Key<User> getUser() {
		return user;
	}

	public void setUser(Key<User> user) {
		this.user = user;
	}

}
