package com.geeklabs.lringer.repository;

import com.geeklabs.lringer.domain.User;
import com.geeklabs.lringer.repository.objectify.ObjectifyCRUDRepository;

public interface UserRepository extends ObjectifyCRUDRepository<User> {
	
	public User getUserByEmail(String userEmail);

	public User getUserByMobileNumber(String mblNum);
}
