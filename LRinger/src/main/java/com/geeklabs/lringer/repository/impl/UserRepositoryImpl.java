package com.geeklabs.lringer.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.lringer.domain.User;
import com.geeklabs.lringer.repository.UserRepository;
import com.geeklabs.lringer.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class UserRepositoryImpl extends AbstractObjectifyCRUDRepository<User> implements UserRepository {

	@Autowired
	private Objectify objectify;

	public UserRepositoryImpl() {
		super(User.class);
	}

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
	
	@Override
	public User getUserByEmail(String userEmail) {
		 User user = objectify
				 			.load()
				 			.type(User.class)
				 			.filter("email", userEmail)
				 			.first()
				 			.now(); // with given email user might not exist, use 'now', if not exist it returns null.
		return user;
	}

	@Override
	public User getUserByMobileNumber(String mblNum) {
		return objectify.load()
						.type(User.class)
						.filter("mobileNumber", mblNum)
						.first()
						.now();
	}
}
