package com.geeklabs.lringer.service.impl;

import java.io.IOException;
import java.util.Collection;

import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.geeklabs.lringer.domain.User;
import com.geeklabs.lringer.dto.TrackDto;
import com.geeklabs.lringer.dto.UserDto;
import com.geeklabs.lringer.facade.UserFacade;
import com.geeklabs.lringer.repository.UserRepository;
import com.geeklabs.lringer.reversegeocoder.model.AddressComponent;
import com.geeklabs.lringer.reversegeocoder.model.GeoCodeResponse;
import com.geeklabs.lringer.reversegeocoder.model.Results;
import com.geeklabs.lringer.service.UserService;
import com.geeklabs.lringer.util.ResponseStatus;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Userinfo;

public class UserServiceImpl implements UserService {

	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private static final JacksonFactory FACTORY = new JacksonFactory();
	
	private UserFacade userFacade;
	private UserRepository userRepository;
	
	public void setUserFacade(UserFacade userFacade) {
		this.userFacade = userFacade;
	}
	
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	@Transactional
	public ResponseStatus updateSigninStatus(long id) {
		User user = userRepository.findOne(id);
		user.setSignIn(false);
		userRepository.save(user);
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@Override
	@Transactional
	public ResponseStatus registerUser(UserDto userDto) throws IOException {
		ResponseStatus responseStatus = new ResponseStatus();
		Userinfo userInfo = getUserInfoByToken(userDto.getAccessToken());
		if (!userInfo.containsKey("error")) {
			// Is user exist
			User user = userRepository.getUserByEmail(userInfo.getEmail());
			
			if (null != user){
				
				if (!user.isSignIn() && user.getMobileNumber().equals("+91" + userDto.getMobileNumber())) {
					user.setSignIn(true);
					responseStatus.setId(user.getId());
					responseStatus.setImageUrl(userInfo.getPicture());
					responseStatus.setUserName(userInfo.getName());
					responseStatus.setStatus("success");
					return responseStatus;
				}else if (!user.isSignIn() && !user.getMobileNumber().equals("+91" + userDto.getMobileNumber())) {
					responseStatus.setStatus("mobile num");
					return responseStatus;
				}else {
					//need to return User alredy exist
					responseStatus.setStatus("with this mobile number User  already exist");
					return responseStatus;
				}
			}

			// Create user 
			user = userFacade.convertToUserDomain(userInfo, userDto);
			userRepository.save(user);
			
			responseStatus.setId(user.getId());
			responseStatus.setImageUrl(userInfo.getPicture());
			responseStatus.setUserName(userInfo.getName());
			responseStatus.setStatus("success");
			return responseStatus;
		}
		return responseStatus;
	}

	private Userinfo getUserInfoByToken(String accessToken) {
		GoogleCredential credential = new GoogleCredential().setAccessToken(accessToken);
		Oauth2 oauth2 = new Oauth2.Builder(HTTP_TRANSPORT, FACTORY, credential).build();
		Userinfo userinfo = null;
		try {
			 return userinfo = oauth2.userinfo().get().setOauthToken(accessToken).execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return userinfo;
	}

	@Override
	@Transactional
	public ResponseStatus updateLocation(TrackDto trackDto, String userId) {
		ResponseStatus responseStatus = new ResponseStatus();
		User user = getUserByUserId(Long.valueOf(userId));
		double longitude = trackDto.getLongitude();
		double latitude = trackDto.getLatitude();
		
		
		if (user != null && latitude > 0 && longitude > 0) {
			user.setLatitude(latitude);
			user.setLongitude(longitude);
			GeoCodeResponse addressFromLatLng = null;
			try {
				addressFromLatLng = getAddressFromLatLng(latitude, longitude);
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if ("OK".equals(addressFromLatLng.getStatus())) {
				for (Results results : addressFromLatLng.getResults()) {
					for (AddressComponent addressComponent : results.getAddress_components()) {
						Collection<String> types = addressComponent.getTypes();
						if (types.isEmpty()) {
							continue;
						} else if (types.contains("route")) {
							user.setAddress(addressComponent.getLong_name());
						} else if (types.contains("sublocality")) {
							String address = user.getAddress();
							if (address != null) {
								user.setAddress(address.concat(",").concat(addressComponent.getLong_name()));
							}
						} else if (types.contains("locality")) {
							String address = user.getAddress();
							if (address != null) {
								user.setAddress(address.concat(",").concat(addressComponent.getLong_name()));
								user.setAddress(address.concat(",").concat(addressComponent.getLong_name()));
							}
						} else if (types.contains("administrative_area_level_2")) {
							user.setCity(addressComponent.getLong_name());
						} else if (types.contains("administrative_area_level_1")) {
							user.setState(addressComponent.getLong_name());
						} else if (types.contains("country")) {
							user.setCountry(addressComponent.getLong_name());
						} else if (types.contains("postal_code")) { // TODO
							// PostalCode postalCode = new PostalCode();
							// postalCode.setPostalCode(addressComponent.getLong_name());
						}
					}
					break;
				}
				userRepository.save(user);
				responseStatus.setStatus("success");
				return responseStatus;
			}
		}
		return responseStatus;
	}

	
	private GeoCodeResponse getAddressFromLatLng(double lattitute, double longtitute) throws ClientProtocolException, IOException {
		String latitude = String.valueOf(lattitute);
		String longitude = String.valueOf(longtitute);
		String URL = "http://maps.google.com/maps/api/geocode/json?latlng=" + latitude + "," + longitude + "&sensor=true";

		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String forObject = restTemplate.getForObject(URL, String.class);
		System.out.println(forObject);
		GeoCodeResponse geoCodeResponse = mapper.readValue(forObject, GeoCodeResponse.class);
		return geoCodeResponse;
	}

	
	@Override
	@Transactional
	public User getUserByUserId(long userId) {
		User user = userRepository.findOne(userId);
		return user;
	}
	
	@Override
	@Transactional
	public UserDto getLocationByMobileNumber(String mblNum) {
		User userByMobileNumber = userRepository.getUserByMobileNumber(mblNum);
		return userFacade.convertToUserDto(userByMobileNumber);
	}
}