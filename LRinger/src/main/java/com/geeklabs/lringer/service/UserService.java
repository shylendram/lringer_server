package com.geeklabs.lringer.service;

import java.io.IOException;

import com.geeklabs.lringer.domain.User;
import com.geeklabs.lringer.dto.TrackDto;
import com.geeklabs.lringer.dto.UserDto;
import com.geeklabs.lringer.util.ResponseStatus;

public interface UserService {
	 ResponseStatus updateSigninStatus(long id);
	 ResponseStatus registerUser(UserDto userDto) throws IOException;
	 User getUserByUserId(long userId);
	UserDto getLocationByMobileNumber(String mblNum);
	
	ResponseStatus updateLocation(TrackDto trackDto, String userId);
}
